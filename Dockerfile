FROM alpine:3.8
LABEL maintainer="ic3b3rg@gmail.com"

# Add dependencies for python packages
# gcc and musl-dev enable python packages to compile c code
#
# The other libraries we install into the container are:
#
# library           required by
# -------           -----------
# libev-dev         bjoern
# libffi-dev        bcrypt
# postgresql-dev    postgres
#
# python3-dev installs Python 3.6.4 on Alpine 3.8

RUN apk update && apk upgrade && apk add \
  gcc \
  libev-dev \
  libffi-dev \
  musl-dev \
  postgresql-dev \
  python3-dev \
&& pip3 install --upgrade pip setuptools
